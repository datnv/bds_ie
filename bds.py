import tensorflow as tf
import numpy as np
from parameters import *
import math

def weight_fc_variable(shape,name):
    std = shape[1]
    std = np.sqrt(2. / std)
    initial = tf.truncated_normal(shape, stddev=std, mean=0.0)
    return tf.Variable(initial,name=name)

def bias_variable(shape,name):
    initial = tf.constant(0., shape=shape)
    return tf.Variable(initial,name=name)

def input():
    x = tf.placeholder(tf.int32, shape=[None],name='x')
    y_ = tf.placeholder(tf.float32, [None, 1],name='y_')

    tf.add_to_collection('x', x)
    tf.add_to_collection('y_', y_)
    return x,y_

def inference(x,y_):
    #embeddings = tf.Variable(
    #    tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0),name='embeddings')
    embeddings = tf.Variable(
        tf.truncated_normal([vocabulary_size, embedding_size], stddev=np.sqrt(2. / vocabulary_size), mean=0.0),name='embeddings')
    embed = tf.nn.embedding_lookup(embeddings, x,name='embed')

    nce_weights = weight_fc_variable([vocabulary_size, embedding_size],'nce_weights')
    nce_biases = bias_variable([vocabulary_size],'nce_biases')

    loss = tf.reduce_mean(
        tf.nn.nce_loss(
            weights=nce_weights,
            biases=nce_biases,
            labels=y_,
            inputs=embed,
            num_sampled=num_sampled,
            num_classes=vocabulary_size))

    current_step=tf.Variable(1)

    increase_step_op = tf.assign(current_step, current_step + 1)

    max_step = (total_words//batch_size)*nb_max_epochs
    learning_rate = tf.maximum(min_lnr,initial_lnr - tf.cast((current_step-1)*(initial_lnr - min_lnr),tf.float32)/(max_step-1))

    #lr = opts.learning_rate * tf.maximum(
    #0.0001, 1.0 - tf.cast(self._words, tf.float32) / words_to_train)

    #learning_rate = tf.Variable(initial_lnr,dtype=tf.float32,trainable=False)
    train_step = tf.train.MomentumOptimizer(learning_rate=learning_rate, momentum=0.9).minimize(loss)

    #add to collection
    tf.add_to_collection('embeddings',embeddings)
    tf.add_to_collection('embed', embed)
    tf.add_to_collection('loss', loss)
    tf.add_to_collection('current_step', current_step)
    tf.add_to_collection('increase_step_op', increase_step_op)
    tf.add_to_collection('train_step', train_step)
    tf.add_to_collection('learning_rate', learning_rate)

#some additional Variable, using when restore model
def define_additional_variables():
    current_epoch=tf.Variable(-1)
    min_loss_tensor=tf.Variable(1000000.,dtype=tf.float32)


    tf.add_to_collection('current_epoch', current_epoch)

    tf.add_to_collection('min_loss_tensor', min_loss_tensor)

    return  current_epoch,min_loss_tensor






