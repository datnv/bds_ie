from builtins import id

import tensorflow as tf
import numpy as np
import os
import bds
from parameters import *
from random import shuffle
import logging
import helper
import random


def permutation(X,Y):
    if len(X)!=0:
        index_list = list(np.random.permutation(len(X)))
        for i in range(5):
            index_list = np.random.permutation(index_list)
        X=X[index_list]
        Y=Y[index_list]
    return X,Y


def train_downsampling(step_when_print_result):
    dic_id_to_keepproba = helper.load_dic('file npy/dic_id_to_keepproba.npy')
    sess= tf.InteractiveSession()
    if not os.path.isfile('save/current/model.ckpt.index'):  # not load
        print('not load parameters')
        x, y_ = bds.input()
        bds.inference(x,y_)
        bds.define_additional_variables()
        sess.run(tf.global_variables_initializer())
        saver = tf.train.Saver()
    else:
        print('load parameters')
        saver = tf.train.import_meta_graph('save/current/model.ckpt.meta')
        saver.restore(sess, "save/current/model.ckpt")

    x=tf.get_collection('x')[0]
    y_ = tf.get_collection('y_')[0]
    loss = tf.get_collection('loss')[0]
    current_epoch = tf.get_collection('current_epoch')[0]
    current_step = tf.get_collection('current_step')[0]
    train_step = tf.get_collection('train_step')[0]
    learning_rate = tf.get_collection('learning_rate')[0]
    min_loss_tensor = tf.get_collection('min_loss_tensor')[0]
    increase_step_op = tf.get_collection('increase_step_op')[0]


    step = current_step.eval()
    lnr = learning_rate.eval()
    min_loss = min_loss_tensor.eval()
    list_train_file_name = []
    repeat = 0
    num_change=0
    for file in os.listdir(train_folder + '/input'):
        list_train_file_name.append(file)

    #with open('file txt/log_training.txt', 'w', encoding='utf8') as fw:
    logging.basicConfig(filename='file txt/log_training.txt', level=logging.INFO)
    logger = logging.getLogger(__name__)
    for epoch in range(current_epoch.eval() + 1, nb_max_epochs):
        logger.info('epoch: '+str(epoch))
        print('epoch: '+str(epoch))
        shuffle(list_train_file_name)
        #for file in list_train_file_name:
        for i in range(90):
            print('load data to memmory ...')
            num_file = len(list_train_file_name)//89
            train_inputs=np.array([])
            train_labels=np.empty((0,1))
            #train_labels=np.array([[]])
            sub_list_file_name = list_train_file_name[i*num_file:(i+1)*num_file]
            h=0
            for file in sub_list_file_name:
                h+=1
                train_inputs = np.append(train_inputs,np.load(train_folder+'/input/'+file))
                train_labels = np.append(train_labels,np.load(train_folder+'/label/'+file),axis=0)

            new_train_inputs = []
            new_train_labels = []

            for row in range(np.shape(train_inputs)[0]):
                keep_prob = dic_id_to_keepproba[train_inputs[row]]*dic_id_to_keepproba[train_labels[row][0]]
                if random.uniform(0, 1) <= keep_prob:
                    new_train_inputs.append(train_inputs[row])
                    new_train_labels.append([train_labels[row][0]])
            train_inputs = np.array(new_train_inputs)
            train_labels = np.array(new_train_labels)
            print('done')

            train_inputs, train_labels = permutation(train_inputs,train_labels)
            average_ttl = []
            for i in range(len(train_inputs) // batch_size + 1):
                step += 1
                x_batch = train_inputs[i * batch_size:(i + 1) * batch_size]
                y_batch = train_labels[i * batch_size:(i + 1) * batch_size]
                ttl, _, _a,lnr_view = sess.run([loss,train_step,increase_step_op,learning_rate], \
                                                feed_dict={x: x_batch, y_: y_batch})
                average_ttl.append(ttl)
                if step % step_when_print_result == 0:
                    average_ttl = np.average(average_ttl)
                    print('step '+str(step)+' , '+'loss: '+str(average_ttl)
                        + ' , lnr: ' + str(lnr_view))
                    logger.info('step '+str(step)+' , '+'loss: '+str(average_ttl)
                        + ' , lnr: ' + str(lnr_view))
                    #sess.run(tf.assign(current_step, step))
                    if average_ttl <= min_loss:
                        min_loss = average_ttl
                        sess.run(tf.assign(min_loss_tensor, min_loss))
                        saver.save(sess, "save/max/model.ckpt")
                        repeat = 0
                    else:
                        repeat+=1
                    saver.save(sess, "save/current/model.ckpt")

                    average_ttl = []

                    '''
                    # consider change learning rate
                    if repeat >= 40:
                        repeat = 0
                        min_loss = average_ttl
                        sess.run(tf.assign(min_loss_tensor, min_loss))
                        lnr /= 3.
                        sess.run(tf.assign(learning_rate, lnr))
                        num_change+=1
                        #if num_change == 7:
                        #    break
                        print('change learning rate = ' + str(lnr))
                    '''

        '''
        lnr /= 3.
        sess.run(tf.assign(learning_rate, lnr))
        num_change += 1
        # if num_change == 7:
        #    break
        print('change learning rate = ' + str(lnr))
        '''

def train(step_when_print_result):
    dic_id_to_keepproba = helper.load_dic('file npy/dic_id_to_keepproba.npy')
    sess= tf.InteractiveSession()
    if not os.path.isfile('save/current/model.ckpt.index'):  # not load
        print('not load parameters')
        x, y_ = bds.input()
        bds.inference(x,y_)
        bds.define_additional_variables()
        sess.run(tf.global_variables_initializer())
        saver = tf.train.Saver()
    else:
        print('load parameters')
        saver = tf.train.import_meta_graph('save/current/model.ckpt.meta')
        saver.restore(sess, "save/current/model.ckpt")

    x=tf.get_collection('x')[0]
    y_ = tf.get_collection('y_')[0]
    loss = tf.get_collection('loss')[0]
    current_epoch = tf.get_collection('current_epoch')[0]
    current_step = tf.get_collection('current_step')[0]
    train_step = tf.get_collection('train_step')[0]
    learning_rate = tf.get_collection('learning_rate')[0]
    min_loss_tensor = tf.get_collection('min_loss_tensor')[0]
    increase_step_op = tf.get_collection('increase_step_op')[0]


    step = current_step.eval()
    lnr = learning_rate.eval()
    min_loss = min_loss_tensor.eval()
    list_train_file_name = []
    repeat = 0
    num_change=0
    for file in os.listdir(train_folder + '/input'):
        list_train_file_name.append(file)

    #with open('file txt/log_training.txt', 'w', encoding='utf8') as fw:
    logging.basicConfig(filename='file txt/log_training.txt', level=logging.INFO)
    logger = logging.getLogger(__name__)
    average_ttl = []
    for epoch in range(current_epoch.eval() + 1, nb_max_epochs):
        logger.info('epoch: '+str(epoch))
        print('epoch: '+str(epoch))
        shuffle(list_train_file_name)
        #for file in list_train_file_name:
        for i in range(80):
            print('load data to memory ...')
            num_file = len(list_train_file_name)//79
            train_inputs=np.array([])
            train_labels=np.empty((0,1))
            #train_labels=np.array([[]])
            sub_list_file_name = list_train_file_name[i*num_file:(i+1)*num_file]
            for file in sub_list_file_name:
                train_inputs = np.append(train_inputs,np.load(train_folder+'/input/'+file))
                train_labels = np.append(train_labels,np.load(train_folder+'/label/'+file),axis=0)
            print('dơne')

            train_inputs, train_labels = permutation(train_inputs, train_labels)
            average_ttl = []
            for i in range(len(train_inputs) // batch_size + 1):
                step += 1
                x_batch = train_inputs[i * batch_size:(i + 1) * batch_size]
                y_batch = train_labels[i * batch_size:(i + 1) * batch_size]
                ttl, _, _a, lnr_view = sess.run([loss, train_step, increase_step_op, learning_rate], \
                                                feed_dict={x: x_batch, y_: y_batch})
                average_ttl.append(ttl)
                if step % step_when_print_result == 0:
                    average_ttl = np.average(average_ttl)
                    print('step ' + str(step) + ' , ' + 'loss: ' + str(average_ttl)
                          + ' , lnr: ' + str(lnr_view))
                    logger.info('step ' + str(step) + ' , ' + 'loss: ' + str(average_ttl)
                                + ' , lnr: ' + str(lnr_view))
                    # sess.run(tf.assign(current_step, step))
                    if average_ttl <= min_loss:
                        min_loss = average_ttl
                        sess.run(tf.assign(min_loss_tensor, min_loss))
                        saver.save(sess, "save/max/model.ckpt")
                        repeat = 0
                    else:
                        repeat += 1
                    saver.save(sess, "save/current/model.ckpt")

                    average_ttl = []

                    '''
                    # consider change learning rate
                    if repeat >= 40:
                        repeat = 0
                        min_loss = average_ttl
                        sess.run(tf.assign(min_loss_tensor, min_loss))
                        lnr /= 3.
                        sess.run(tf.assign(learning_rate, lnr))
                        num_change+=1
                        #if num_change == 7:
                        #    break
                        print('change learning rate = ' + str(lnr))
                    '''

        '''
        lnr /= 3.
        sess.run(tf.assign(learning_rate, lnr))
        num_change += 1
        # if num_change == 7:
        #    break
        print('change learning rate = ' + str(lnr))
        '''

if __name__ == "__main__":
    train(500000)
