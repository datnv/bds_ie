from parameters import *
import tensorflow as tf
import helper
import os
import numpy as np
import random

file_index = 1
numpost=0
numsamples=0

def bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

#def bytes_feature(value):
#    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def save_to_tfrecord(file_path,dic_word_to_id,dic_id_to_word,num_post_per_file):
    numpost=0
    file_index = 1
    file_name = 'file tfrecord/train/'+ str(file_index) + '.tfrecords'
    writer = tf.python_io.TFRecordWriter(file_name)
    post = []
    with open(file_path, encoding="utf8") as f:
        for line in f:
            if '==========' in line:
                post_context = ''.join(post)
                list_sentences = helper.post_to_list_sentences(post_context)

                for sent in list_sentences:
                    for i in range(len(sent)):
                        input = i
                        input_raw = np.array(input).tostring()
                        #input_raw = bytes(input)
                        #input_raw = input
                        list_index = range(max(i-window_size,0),min(i+window_size+1,len(sent)))
                        for index in list_index:
                            if index != i:
                                label=None
                                if sent[index] in dic_word_to_id:
                                    label=dic_word_to_id[sent[index]]
                                else:
                                    label=0
                                #input_raw = input.tostring()
                                #label_raw = label.tostring()
                                label_raw = np.array(label).tostring()
                                #label_raw = bytes(label)
                                #label_raw=label
                                feature = {'label_raw': bytes_feature(label_raw),
                                           'input_raw': bytes_feature(input_raw)}
                                example = tf.train.Example(features=tf.train.Features(feature=feature))
                                #writer.write(example.SerializeToString())
                post = []
                numpost += 1
                print(numpost)
                if numpost % num_post_per_file == 0:
                    writer.close()
                    file_index += 1
                    file_name = 'file tfrecord/train/' + str(file_index) + '.tfrecords'
                    writer = tf.python_io.TFRecordWriter(file_name)
            else:
                post.append(line)
    writer.close()


def save_to_npy(file_path,dic_word_to_id,dic_id_to_word,dic_id_to_keepproba,num_post_per_file,is_subsampling):
    global numpost
    global file_index
    global numsamples
    train_inputs = []
    train_labels = []
    #writer = tf.python_io.TFRecordWriter(file_name)
    post = []
    with open(file_path, encoding="utf8") as f:
        for line in f:
            if '==========' in line:
                post_context = ''.join(post)
                list_sentences = helper.post_to_list_sentences(post_context)

                for sent in list_sentences:
                    for i in range(len(sent)):
                        input=None
                        if sent[i] in dic_word_to_id:
                            input = dic_word_to_id[sent[i]]
                        else:
                            input = 0
                        proba_input = dic_id_to_keepproba[input]
                        list_index = range(max(i-window_size,0),min(i+window_size+1,len(sent)))
                        for index in list_index:
                            if index != i:
                                label=None
                                if sent[index] in dic_word_to_id:
                                    label=dic_word_to_id[sent[index]]
                                else:
                                    label=0
                                if is_subsampling:
                                    proba_output = dic_id_to_keepproba[label]
                                    if proba_input >= 1.0 and proba_output >= 1.0:
                                        train_inputs.append(input)
                                        train_labels.append(label)
                                        numsamples+=1
                                    else:
                                        keep_proba = proba_input*proba_output
                                        if random.uniform(0, 1) <= keep_proba:
                                            train_inputs.append(input)
                                            train_labels.append(label)
                                            numsamples+=1
                                else:
                                    train_inputs.append(input)
                                    train_labels.append(label)
                                    numsamples += 1

                post = []
                numpost += 1
                print(numpost)
                if numpost % num_post_per_file == 0:
                    np.save('file npy/train/input/'+ str(file_index) + '.npy',np.array(train_inputs))
                    np.save('file npy/train/label/'+ str(file_index) + '.npy',np.array(train_labels).reshape((len(train_labels),1)))
                    train_inputs = []
                    train_labels = []
                    file_index += 1
            else:
                post.append(line)
    np.save('file npy/train/input/' + str(file_index) + '.npy', np.array(train_inputs))
    np.save('file npy/train/label/' + str(file_index) + '.npy', np.array(train_labels).reshape((len(train_labels), 1)))


def remove_files_if_exsist():
    print('remove old files')
    for file in os.listdir('file tfrecord/train'):
        os.remove('file tfrecord/train/'+file)
    for file in os.listdir('file npy/train/input'):
        os.remove('file npy/train/input/'+file)
    for file in os.listdir('file npy/train/label'):
        os.remove('file npy/train/label/'+file)


if __name__ == "__main__":
    folder = '../DATA_BDS_NEW/language_model/language_model'
    num_post_per_file=2000
    dic_word_to_id = helper.load_dic('file npy/dic_word_to_id.npy')
    dic_id_to_word = helper.load_dic('file npy/dic_id_to_word.npy')
    dic_id_to_keepproba = helper.load_dic('file npy/dic_id_to_keepproba.npy')

    remove_files_if_exsist()

    for file in os.listdir(folder):
        print(file)
        file_path=folder+'/'+file
        save_to_npy(file_path,dic_word_to_id,dic_id_to_word,dic_id_to_keepproba,num_post_per_file,is_subsampling=True)
    print('numsamples: '+str(numsamples))
