from pyvi.pyvi import ViTokenizer
import os
import helper

folder_input = 'D:/DATA BDS NEW/original_data'
folder_output='D:/DATA BDS NEW/language_model'

for file in os.listdir(folder_input):
    print(file)
    file_path=folder_input+'/'+file
    #post=[]
    with open(file_path,encoding="utf8") as f, open(folder_output+'/'+file,'w',encoding='utf8') as fw:
        for line in f:
            if '==========' in line:
                fw.write(line)
            else:
                line = helper.remove_html_tag(line)
                new_sent = ViTokenizer.tokenize(line)
                if new_sent[-1]!='\n':
                    new_sent = new_sent+'\n'
                fw.write(new_sent)
