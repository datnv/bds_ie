import re
import numpy as np

vietnamese_letters = "ẹìơẽgốễjỷùầêẩđàỗắẵúọõăưỏòỳqpợỵủâlằcửồấeậbềỹảữứdệẳfaởẫrnkiwỉĩểừạáỡụựéãmặôzớíhyxèoịuýờũẻộóếtổsv"

number_pattern = re.compile(
        "[\,\.]?[0-9]+[\,\.]?[\s]*[0-9]*[\,\.]?[\s]*[0-9]*[\,\.]?\\Z")

def get_type_number(number_string):
    # if number string is phone number
    if len(number_string)>=10:
        return ' xnum3x '
    # if number string is float
    if len(re.findall('\.|\,',number_string)) > 0:
        return ' xnum2x '
    #if number string is integer
    return ' xnum1x '

def replace_number_in_sentence(sentence):
    list_number_location = []
    i=0;
    while i < len(sentence):
        longest_number = None
        j=i;
        while j < len(sentence):
        #for j in range(len(sentence) - i):
            s = sentence[i:j+1]
            if bool(re.match(number_pattern,s)):
                longest_number = s
            else:
                break
            j+=1
        if longest_number != None:
            type_nb = get_type_number(sentence[i:j])
            list_number_location.append([type_nb,(i,j)])
            i = j
        else:
            i+=1
    res=[]
    j=0
    end=None
    for i in range(len(list_number_location)):
        start=list_number_location[i][1][0]
        end=list_number_location[i][1][1]
        type_nb = list_number_location[i][0]
        #print(type_nb)
        res.append(sentence[j:start]+type_nb)
        j=end
    if end!=None:
        res.append(sentence[end:])
        return ''.join(res)
    else:
        return sentence

def replace_email_in_sentence(sentence):
    sentence = re.sub('[\w\.-]+\s*@\s*([\w\.-]+\.[\w\.-]+)',' xemailöx ',sentence)
    return sentence

def replace_url_in_sentence(sentence):
    sentence=re.sub('((https?\s*:\s*/\s*/\s*)|(www\.))(?:[-\w.]|(?:%[\da-fA-F]{2}))+(\s*/\s*([a-z]+\.[a-z]+)*)*',' xurlöx ',sentence)
    return sentence


def my_replace1(match):
    match = match.group()
    return ' '+str(match)

def my_replace2(match):
    match = match.group()
    return str(match)+' '

def replace_punctuation_in_sentence(sentence):
    punc = '\!\"\#\$\%\&\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\\\^\`\]\{\|\}\~\”\“\—'
    sentence = re.sub('(?<=['+vietnamese_letters+'])['+punc+']',my_replace1,sentence)
    sentence = re.sub('['+punc+'](?=['+vietnamese_letters+'])',my_replace2,sentence)
    return sentence

def split_text_into_sentences(text):
    text = text.strip()
    text = text.replace(u'....', u'. ')
    text = text.replace(u'...', u'. ')
    text = text.replace(u'..', u'. ')
    text = text.replace(u'…', u'. ')
    text = re.sub('[\.\!\?](\s(?=\D)|\\Z)', '\n', text)
    list_lines = re.split("\n|\r|\r\n",text)
    return list_lines

def split_sentence_to_words(sentence):
    sentence = sentence.lower()
    sentence = sentence.strip()
    sentence = replace_email_in_sentence(sentence)
    sentence = replace_url_in_sentence(sentence)
    sentence = replace_number_in_sentence(sentence)
    sentence = replace_punctuation_in_sentence(sentence)
    return sentence.split()



def remove_html_tag(text):
    cleanr = re.compile('<.*?>')
    text = re.sub(cleanr, ' ', text)
    return text

def post_to_list_sentences(post):
    text = remove_html_tag(post)
    list_sentences = split_text_into_sentences(text)
    result = []
    for sentence in list_sentences:
        if len(sentence)!=0:
            result.append(split_sentence_to_words(sentence))
    return result
'''
text="""
Cho thuê phòng trọ mặt_tiền đường_nhựa 12m - Tây Hòa - quận 9 - 3tr/tháng - bao điện nước
Vị trí phòng trọ:
- Mặt tiền đường_nhựa 12m.
- Cách chợ Tây Hoà 100m.
- Bán kính 500m đầy_đủ tiện ích: Trường học mẫu giáo, trường cấp 1, trường cấp 2, trường cấp 3, chợ, siêu thị...

Tiện ích phòng trọ:
- Bao điện nước.
- Đầy đủ tiện nghi: Giường, quạt, tủ, máy lạnh, máy giặt.
- Có nhà_vệ_sinh riêng.
- Có chỗ để xe thoải mái.
- Có lắp camera an ninh.
- Wifi tốc_độ cao.
"""
#sent = "Giá thuê: 2.5 triệu/tháng. LH: Mr Trường 0917. 226. 568."

list_sents = post_to_list_sentences(text)

for sent in list_sents:
    print(sent)
'''


def save_dic(dic,path):
    print('save dic to '+path)
    list=[]
    for key in dic.keys():
        list.append([key,dic[key]])
    #list = np.array(list)
    np.save(path, np.array(list,dtype=object))
    print(type(list[0][0]))
    print(type(list[0][1]))
    print('done')

def load_dic(path):
    print('load dic in '+path)
    list=np.load(path)
    print(type(list[0][0]))
    print(type(list[0][1]))
    dic={}
    for i in range(len(list)):
        dic[list[i][0]]=list[i][1]
    return dic

def save_set(set,path):
    print('save set to '+path)
    list=[]
    for ele in set:
        list.append(ele)
    np.save(path,np.array(list,dtype=object))
    print(type(list[0]))
    print('done')

def load_set(path):
    print('load set in '+path)
    list=np.load(path)
    print(type(list[0]))
    myset=set()
    for i in range(len(list)):
        myset.add(list[i])
    return myset


def replace_line_with_frequent_phrase(line,dictionary):
    #[\,\.\!\?\:\;]
    punc=[',','.','!','?',':',';','(',')','-']
    rs=[]
    i=0
    line = remove_html_tag(line).lower()
    line=line.split()
    while i < len(line):
    #for i in range(len(sentence)-1):
        if i==len(line)-1:
            rs.append(line[i])
            i+=1
        else:
            p=''
            s=''
            pair='_'.join([line[i],line[i+1]])
            if pair[0] in punc:
                p=pair[0]
                pair=pair[1:]
            if pair[-1] in punc:
                s=pair[-1]
                pair = pair[:len(pair)-1]
            if pair in dictionary:
                rs.append(''.join([p,pair,s]))
                i+=2
            else:
                rs.append(line[i])
                i+=1
    rs.append('\n')
    return ' '.join(rs)
'''
def replace_sentence_with_unknown_word(sentence,dictionary):
    rs=[]
    i=0
    while i < len(sentence):
    #for i in range(len(sentence)-1):
    return rs
'''