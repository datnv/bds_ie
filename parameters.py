import numpy as np

window_size=5
embedding_size=300 # Dimension of the embedding vector.
batch_size=128
vocabulary_size = np.load('file npy/vocabulary_size.npy')[0]
print(vocabulary_size)
#vocabulary_size = 57544
num_sampled = 100  # Number of negative examples to sample.
initial_lnr = 0.1 # initial learning rate
min_lnr=initial_lnr/1000.

train_folder='file npy/train'

nb_max_epochs=5
total_words = np.load('file npy/total_words.npy')[0]
