import helper
import os
import operator

folder='D:/DATA BDS/language_model'
dic_count_word_name='dic_count_word1.npy'
dic_count_pair_name='dic_count_pair1.npy'
dic_score_pair_name='dic_score_pair1.npy'


#folder='D:/DATA BDS/title_description'
vietnamese_letters = "ẹìơẽgốễjỷùầêẩđàỗắẵúọõăưỏòỳqpợỵủâlằcửồấeậbềỹảữứdệẳfaởẫrnkiwỉĩểừạáỡụựéãmặôzớíhyxèoịuýờũẻộóếtổsv"
dic_count_word={}
dic_count_pair={}
delta=20
numpost=0



def check_valid_pair(pair):
    rs=True
    for c in pair:
        if not(c in vietnamese_letters or c=='_'):
            rs = False
            break
    return rs

def count_pair_post(post):
    list_sentences = helper.post_to_list_sentences(post)
    for sent in list_sentences:
        for i in range(len(sent)):
            cr_word = sent[i]
            if check_valid_pair(cr_word):
                if cr_word in dic_count_word.keys():
                    dic_count_word[cr_word]+=1
                else:
                    dic_count_word[cr_word]=1

            if i<len(sent)-1:
                pair='__'.join([sent[i],sent[i+1]])
                if check_valid_pair(pair):
                    if pair in dic_count_pair.keys():
                        dic_count_pair[pair]+=1
                    else:
                        dic_count_pair[pair]=1



if not os.path.exists('file npy/'+str(dic_count_word_name)):

    for file in os.listdir(folder):
        print(file)
        file_path=folder+'/'+file
        post=[]
        with open(file_path,encoding="utf8") as f:
            for line in f:
                if '==========' in line:
                    post_context = ''.join(post)
                    count_pair_post(post_context)
                    post=[]
                    numpost+=1
                else:
                    post.append(line)

    print('num post: '+str(numpost))

    helper.save_dic(dic_count_word,'file npy/'+str(dic_count_word_name))
    helper.save_dic(dic_count_pair,'file npy/'+str(dic_count_pair_name))
    sorted_x = sorted(dic_count_pair.items(), key=operator.itemgetter(1),reverse=True)
    sorted_x1 = sorted(dic_count_word.items(), key=operator.itemgetter(1), reverse=True)

    with open('D:/DATA BDS/frequent_phrase/fq_pair.txt','w',encoding='utf8') as f, open('D:/DATA BDS/frequent_phrase/fq_word.txt','w',encoding='utf8') as f1:
        for tuple in sorted_x:
            f.write(str(tuple[0])+'\t'+str(tuple[1])+'\n')
        for tuple in sorted_x1:
            f1.write(str(tuple[0])+'\t'+str(tuple[1])+'\n')
else:

    dic_count_word = helper.load_dic('file npy/'+str(dic_count_word_name))
    dic_count_pair = helper.load_dic('file npy/'+str(dic_count_pair_name))

print('compute score of pairs')
dic_score_pair={}
for pair in list(dic_count_pair.keys()):
    #print(pair)
    #[word1,word2]=pair.split('__')
    list_word=pair.split('__')
    word1 = list_word[0]
    word2 = list_word[1]
    if word1 in dic_count_word and word2 in dic_count_word:
        score = (dic_count_pair[pair]-delta)/float(dic_count_word[word1]*dic_count_word[word2])
        pair = pair.replace('__','_')
        if pair in dic_score_pair:
            if score > dic_score_pair[pair]:
                dic_score_pair[pair]=score
        else:
            dic_score_pair[pair]=score

helper.save_dic(dic_score_pair,'file npy/'+str(dic_score_pair_name))
sorted_x = sorted(dic_score_pair.items(), key=operator.itemgetter(1),reverse=True)
with open('D:/DATA BDS/frequent_phrase/pair_score.txt','w',encoding='utf8') as f:
    for tuple in sorted_x:
        f.write(str(tuple[0]) + '\t' + str(tuple[1]) + '\n')



