import os
import operator
import helper

folder_input='D:/DATA BDS/language_model'
folder_output='D:/DATA BDS/language_model1'

set_phrases = helper.load_set('file npy/set_phrases1.npy')



for file in os.listdir(folder_input):
    print(file)
    file_path=folder_input+'/'+file
    post=[]
    with open(file_path,encoding="utf8") as f, open(folder_output+'/'+file,'w',encoding='utf8') as fw:
        for line in f:
            if '==========' in line:
                fw.write(line)
            else:
                new_sent = helper.replace_line_with_frequent_phrase(line,set_phrases)
                fw.write(new_sent)

#sent = ['hôm','nay','quận','hoàng','mai','thật','đẹp']
#print(helper.replace_sentence_with_frequent_phrase(sent,set_phrases))

