import gzip
import os
import json

file_input='D:\download ver 2\productPublishDeduplicatedTitleDescription.gz'

folder_output='D:\DATA BDS NEW/original_data'

num_post_per_file = 202738
numpost=0
index_file=0
with gzip.open(file_input,mode='rb') as f:
    writer = open(folder_output+'/'+str(index_file)+'.txt','w',encoding='utf-8')
    for line in f:
        line = line.decode('utf-8')
        data = json.loads(line)
        title = data['title']
        description = data['description']
        writer.write(title+'\n')
        writer.write(description+'\n')
        writer.write('=========='+'\n')
        numpost+=1
        if numpost % num_post_per_file == 0:
            writer.close()
            index_file+=1
            writer = open(folder_output+'/'+str(index_file)+'.txt','w',encoding='utf-8')
    writer.close()

print('num post: '+str(numpost))

