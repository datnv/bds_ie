import helper

score_threshold=5.294757956012409e-07
dic_score_pair_name='dic_score_pair1.npy'
set_phrases_name = 'set_phrases1.npy'

dic_score_pair = helper.load_dic('file npy/'+str(dic_score_pair_name))

set_phrases=set()

for pair in list(dic_score_pair.keys()):
    if dic_score_pair[pair] >= score_threshold:
        set_phrases.add(pair)

helper.save_set(set_phrases,'file npy/'+str(set_phrases_name))



