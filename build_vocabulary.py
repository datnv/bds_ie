import helper
import os
import operator
import numpy as np
import math

frequent_threshold = 11

total_word  = 0

folder='D:/DATA BDS NEW/language_model'
folder_output='D:/DATA BDS NEW/language_model1'
dic_word_to_id = {}
dic_id_to_word={}
dic_id_to_keepproba={}

dic_count_word = {}
if not os.path.isfile('file npy/dic_count_word_vocabulary.npy'):  # not load
    print('build vocabulary')
    for file in os.listdir(folder):
        print(file)
        file_path = folder + '/' + file
        post = []
        with open(file_path, encoding="utf8") as f:
            for line in f:
                if '==========' in line:
                    post_context = ''.join(post)
                    list_sentences = helper.post_to_list_sentences(post_context)
                    for sent in list_sentences:
                        for word in sent:
                            if word in dic_count_word:
                                dic_count_word[word]+=1
                            else:
                                dic_count_word[word]=1
                            total_word+=1
                    post = []
                else:
                    post.append(line)
    helper.save_dic(dic_count_word, 'file npy/dic_count_word_vocabulary.npy')
else:
    print('load dic_count_word')
    dic_count_word = helper.load_dic('file npy/dic_count_word_vocabulary.npy')
    for word in list(dic_count_word.keys()):
        total_word+=dic_count_word[word]

print('total word: '+str(total_word))

sorted_x = sorted(dic_count_word.items(), key=operator.itemgetter(1),reverse=True)

with open('D:/DATA BDS/frequent_phrase/vocabulary_frequent.txt','w',encoding='utf8') as f:
    for tuple in sorted_x:
        f.write(str(tuple[0]) + '\t' + str(tuple[1]) + '\n')
#dic_count_word

dic_word_to_id['xunköx']=0
dic_id_to_word[0]='xunköx'
unknown_count = 0
id=1
for tuple in sorted_x:
    word = tuple[0]
    num_occur = tuple[1]
    if num_occur < frequent_threshold:
        break
    dic_word_to_id[word]=id
    dic_id_to_word[id]=word
    z = dic_count_word[word]/float(total_word)
    #dic_id_to_keepproba[id] = 1 -((z-1e-5)/z - math.sqrt(1e-5/z))
    dic_id_to_keepproba[id] = (math.sqrt(z/0.001)+1)*0.001/z
    id+=1

#z = unknown_count/float(total_word)
#dic_id_to_keepproba[0] =  1 -((z-1e-5)/z - math.sqrt(1e-5/z))
dic_id_to_keepproba[dic_word_to_id['xunköx']]=1.0
dic_id_to_keepproba[dic_word_to_id['xnum1x']]=1.0
dic_id_to_keepproba[dic_word_to_id['xnum2x']]=1.0
dic_id_to_keepproba[dic_word_to_id['xnum3x']]=1.0
dic_id_to_keepproba[dic_word_to_id['xemailöx']]=1.0
dic_id_to_keepproba[dic_word_to_id['xurlöx']]=1.0

helper.save_dic(dic_word_to_id,'file npy/dic_word_to_id.npy')
helper.save_dic(dic_id_to_word,'file npy/dic_id_to_word.npy')
helper.save_dic(dic_id_to_keepproba,'file npy/dic_id_to_keepproba.npy')


vocabulary_size = len(list(dic_word_to_id.keys()))
np.save('file npy/vocabulary_size.npy',[vocabulary_size])

sorted_x = sorted(dic_word_to_id.items(), key=operator.itemgetter(1))

with open('D:/DATA BDS/frequent_phrase/vocabulary_id.txt','w',encoding='utf8') as f:
    for tuple in sorted_x:
        f.write(str(tuple[0]) + '\t' + str(tuple[1]) + '\n')

with open('D:/DATA BDS/frequent_phrase/keep_proba.txt','w',encoding='utf8') as f:
    for i in range(vocabulary_size):
        f.write(str(i) + '\t' + str(dic_id_to_keepproba[i]) + '\n')


'''

print('replace unknown word in text')
for file in os.listdir(folder):
    print(file)
    file_path=folder+'/'+file
    post=[]
    with open(file_path,encoding="utf8") as f, open(folder_output+'/'+file,'w',encoding='utf8') as fw:
        for line in f:
            if '==========' in line:
                post_context = ''.join(post)
                list_sentences = helper.post_to_list_sentences(post_context)
                for sent in list_sentences:
                    new_sent = helper.replace_sentence_with_frequent_phrase(sent,set_phrases)
                    new_sent=' '.join(new_sent)
                    fw.write(new_sent+'\n')
                fw.write('=========='+'\n')
                post=[]
            else:
                post.append(line)
'''

