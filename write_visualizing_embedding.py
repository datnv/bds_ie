import tensorflow as tf
from parameters import *
import helper
import os
import re
from tensorflow.contrib.tensorboard.plugins import projector

for file in os.listdir('summary'):
    os.remove('summary/' + file)


sess = sess= tf.InteractiveSession()
saver = tf.train.import_meta_graph('save/current/model.ckpt.meta')
saver.restore(sess, "save/current/model.ckpt")

embeddings = tf.get_collection('embeddings')[0]

dic_id_to_word = helper.load_dic('file npy/dic_id_to_word.npy')

writer = tf.summary.FileWriter('summary', sess.graph)
# Write corresponding labels for the embeddings.
with open('summary/metadata.tsv', 'w',encoding='utf8') as f:
    for i in range(vocabulary_size):
        if re.match('\s',dic_id_to_word[i],) :
            print('yes')
        f.write(dic_id_to_word[i]+'.'+'\n')

saver.save(sess, "summary/model.ckpt")

# Create a configuration for visualizing embeddings with the labels in TensorBoard.
config = projector.ProjectorConfig()
embedding_conf = config.embeddings.add()
embedding_conf.tensor_name = embeddings.name
embedding_conf.metadata_path = os.path.join('D://PycharmProjects//bds_ie//summary', 'metadata.tsv')
projector.visualize_embeddings(writer, config)

writer.close()

# Visualize the embeddings.

# Compute the cosine similarity between minibatch examples and all embeddings.
norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings), 1, keep_dims=True))
normalized_embeddings = embeddings / norm

final_embeddings = normalized_embeddings.eval()

# pylint: disable=missing-docstring
# Function to draw visualization of distance between embeddings.
def plot_with_labels(low_dim_embs, labels, filename):
  assert low_dim_embs.shape[0] >= len(labels), 'More labels than embeddings'
  plt.figure(figsize=(18, 18))  # in inches
  for i, label in enumerate(labels):
    x, y = low_dim_embs[i, :]
    plt.scatter(x, y)
    plt.annotate(
        label,
        xy=(x, y),
        xytext=(5, 2),
        textcoords='offset points',
        ha='right',
        va='bottom')

  plt.savefig(filename)


# pylint: disable=g-import-not-at-top
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
from tempfile import gettempdir

tsne = TSNE(
    perplexity=30, n_components=2, init='pca', n_iter=5000, method='exact')
plot_only = 500
low_dim_embs = tsne.fit_transform(final_embeddings[:plot_only, :])
labels = [dic_id_to_word[i] for i in range(plot_only)]
plot_with_labels(low_dim_embs, labels, os.path.join(gettempdir(), 'tsne.png'))

